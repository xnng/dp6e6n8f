const mysql = require('mysql2/promise')
const logger = require('../log/logger')
const mysqlCRUD = require('./crud')

const pool = mysql.createPool({
  host: 'ali.xnngs.cn',
  user: 'root',
  port: 3306,
  password: process.env.DOCKERHUB_MYSQL_PASSWORD,
  database: 'dockerhub',
  charset: 'utf8',
  dateStrings: true,
  waitForConnections: true, 
   connectionLimit: 50,
  queueLimit: 0
})

mysqlCRUD.use(pool, ({ sql, time, error }) => {
  error ? logger.error({ sql, time, error }) : logger.text({ sql, time })
})

module.exports = pool
